##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=libcore
ConfigurationName      :=Debug
WorkspacePath          := "c:\projects\Core"
ProjectPath            := "c:\projects\Core\libcore"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=carlokok
Date                   :=02/08/14
CodeLitePath           :="C:\Program Files (x86)\CodeLite"
LinkerName             :=gcc
SharedObjectLinkerName :=gcc -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/lib$(ProjectName).a
Preprocessors          :=$(PreprocessorSwitch)USE_BOEHM $(PreprocessorSwitch)GC_THREADS 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="libcore.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=windres
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). $(IncludeSwitch)../lib/gc/include $(IncludeSwitch)../lib/libuv/include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := gcc
CC       := gcc
CXXFLAGS :=  -g $(Preprocessors)
CFLAGS   :=  -g $(Preprocessors)
ASFLAGS  := 
AS       := as


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files (x86)\CodeLite
UNIT_TEST_PP_SRC_DIR:=C:\UnitTest++-1.3
Objects0=$(IntermediateDirectory)/core_gc_boehm$(ObjectSuffix) $(IntermediateDirectory)/core_thread$(ObjectSuffix) $(IntermediateDirectory)/core_syncobjs$(ObjectSuffix) $(IntermediateDirectory)/core_debug$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild
all: $(IntermediateDirectory) $(OutputFile)

$(OutputFile): $(Objects)
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(AR) $(ArchiveOutputSwitch)$(OutputFile) @$(ObjectsFileList) $(ArLibs)
	@$(MakeDirCommand) "c:\projects\Core/.build-debug"
	@echo rebuilt > "c:\projects\Core/.build-debug/libcore"

./Debug:
	@$(MakeDirCommand) "./Debug"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/core_gc_boehm$(ObjectSuffix): core_gc_boehm.c $(IntermediateDirectory)/core_gc_boehm$(DependSuffix)
	$(CC) $(SourceSwitch) "c:/projects/Core/libcore/core_gc_boehm.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/core_gc_boehm$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/core_gc_boehm$(DependSuffix): core_gc_boehm.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/core_gc_boehm$(ObjectSuffix) -MF$(IntermediateDirectory)/core_gc_boehm$(DependSuffix) -MM "core_gc_boehm.c"

$(IntermediateDirectory)/core_gc_boehm$(PreprocessSuffix): core_gc_boehm.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/core_gc_boehm$(PreprocessSuffix) "core_gc_boehm.c"

$(IntermediateDirectory)/core_thread$(ObjectSuffix): core_thread.c $(IntermediateDirectory)/core_thread$(DependSuffix)
	$(CC) $(SourceSwitch) "c:/projects/Core/libcore/core_thread.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/core_thread$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/core_thread$(DependSuffix): core_thread.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/core_thread$(ObjectSuffix) -MF$(IntermediateDirectory)/core_thread$(DependSuffix) -MM "core_thread.c"

$(IntermediateDirectory)/core_thread$(PreprocessSuffix): core_thread.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/core_thread$(PreprocessSuffix) "core_thread.c"

$(IntermediateDirectory)/core_syncobjs$(ObjectSuffix): core_syncobjs.c $(IntermediateDirectory)/core_syncobjs$(DependSuffix)
	$(CC) $(SourceSwitch) "c:/projects/Core/libcore/core_syncobjs.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/core_syncobjs$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/core_syncobjs$(DependSuffix): core_syncobjs.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/core_syncobjs$(ObjectSuffix) -MF$(IntermediateDirectory)/core_syncobjs$(DependSuffix) -MM "core_syncobjs.c"

$(IntermediateDirectory)/core_syncobjs$(PreprocessSuffix): core_syncobjs.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/core_syncobjs$(PreprocessSuffix) "core_syncobjs.c"

$(IntermediateDirectory)/core_debug$(ObjectSuffix): core_debug.c $(IntermediateDirectory)/core_debug$(DependSuffix)
	$(CC) $(SourceSwitch) "c:/projects/Core/libcore/core_debug.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/core_debug$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/core_debug$(DependSuffix): core_debug.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/core_debug$(ObjectSuffix) -MF$(IntermediateDirectory)/core_debug$(DependSuffix) -MM "core_debug.c"

$(IntermediateDirectory)/core_debug$(PreprocessSuffix): core_debug.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/core_debug$(PreprocessSuffix) "core_debug.c"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) $(IntermediateDirectory)/core_gc_boehm$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/core_gc_boehm$(DependSuffix)
	$(RM) $(IntermediateDirectory)/core_gc_boehm$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/core_thread$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/core_thread$(DependSuffix)
	$(RM) $(IntermediateDirectory)/core_thread$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/core_syncobjs$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/core_syncobjs$(DependSuffix)
	$(RM) $(IntermediateDirectory)/core_syncobjs$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/core_debug$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/core_debug$(DependSuffix)
	$(RM) $(IntermediateDirectory)/core_debug$(PreprocessSuffix)
	$(RM) $(OutputFile)
	$(RM) $(OutputFile)
	$(RM) "../.build-debug/libcore"


