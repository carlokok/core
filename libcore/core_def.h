#ifndef core_def_h
#define core_def_h
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#if _WIN32 || _WIN64
   #if _WIN64
     #define IS64BITS
  #else
    #define IS32BITS
  #endif
#endif

// Check GCC
#if __GNUC__
  #if __x86_64__ || __ppc64__ || __aarch64__
    #define IS64BITS
  #else
    #define IS32BITS
  #endif
#endif


#endif //core_def_h