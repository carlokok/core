@echo off
mkdir d2
set PATH=C:\mingw64\bin;%PATH%


gcc -m64 -c  "c:/projects/Core/libcore/core_gc_boehm.c" -g -DUSE_BOEHM -DGC_THREADS  -o ./d2/core_gc_boehm.o -I. -I. -I../lib/gc/include -I../lib/libuv/include
gcc -m64 -c  "c:/projects/Core/libcore/core_thread.c" -g -DUSE_BOEHM -DGC_THREADS  -o ./d2/core_thread.o -I. -I. -I../lib/gc/include -I../lib/libuv/include
gcc -m64 -c  "c:/projects/Core/libcore/core_debug.c" -g -DUSE_BOEHM -DGC_THREADS  -o ./d2/core_debug.o -I. -I. -I../lib/gc/include -I../lib/libuv/include
gcc -m64 -c  "c:/projects/Core/libcore/core_syncobjs.c" -g -DUSE_BOEHM -DGC_THREADS  -o ./d2/core_syncobjs.o -I. -I. -I../lib/gc/include -I../lib/libuv/include


ar rcus ./D2/liblibcore.a ./D2/core_gc_boehm.o ./D2/core_thread.o    ./d2/core_syncobjs.o ./d2/core_debug.o

pause