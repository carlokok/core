#ifdef USE_BOEHM
#include "gc.h" // redirs the createthread apis to the gc
#endif
#include "core.h"
#include "assert.h"
#include "stdio.h"


#if defined(_WIN32) || defined(_WIN64)
#include "windows.h"

uint32_t core_thread_currentthreadid()
{
	return GetCurrentThreadId();
}

THREADHANDLE core_thread_gethandletoid(uint32_t th)
{
	return OpenThread(THREAD_ALL_ACCESS, FALSE, th);
}

typedef struct CoreThreadData {
	core_thread_threadproc proc;
	void* context;
	} CoreThreadData;

DWORD WINAPI CoreStartProc(LPVOID value) {
	CoreThreadData* data = (CoreThreadData*)value;
	assert(data && "data not set");
	core_thread_threadproc proc = data->proc;
	void* context = data->context;
	free(data);
	ExitThread(proc(context));
}

THREADHANDLE core_thread_create(core_thread_threadproc proc, void* context){
	assert(proc && "no proc set");
	CoreThreadData* data = malloc(sizeof(CoreThreadData));
	data->proc = proc;
	data->context = context;
	return CreateThread(NULL, 0, CoreStartProc, data, CREATE_SUSPENDED, NULL);
}

  
void core_thread_closehandle(THREADHANDLE handle)
{
	printf("Closing thread handle: %d\n", handle);
	CloseHandle(handle);
}
	
void core_thread_suspend(THREADHANDLE handle)
{
	SuspendThread(handle);
}
void core_thread_resume(THREADHANDLE handle)
{
	ResumeThread(handle);
}	

int core_thread_wait(THREADHANDLE handle, int timeoutms)
{
	return WAIT_OBJECT_0 == WaitForSingleObject(handle, timeoutms < 0 ? INFINITE: timeoutms);
}

int core_thread_yield()
{
	SwitchToThread();
}

void Thread_Sleep_internal(uint32_t msec) {
	Sleep(msec);
}



uint64_t DateTime_GetNow()
{
	uint64_t res;
	GetSystemTimeAsFileTime ((FILETIME*) &res);
	return res + 504911232000000000ull; 
}


uint64_t Stopwatch_GetTimestamp() {
	static uint64_t stopwatch_frequency = 0;
	if (stopwatch_frequency == 0) {
		QueryPerformanceFrequency((PLARGE_INTEGER)&stopwatch_frequency);
	}
	uint64_t res;
	QueryPerformanceCounter ((PLARGE_INTEGER)&res);

	return res * 10000000.0 / stopwatch_frequency;
}


#endif