#ifndef core_h
#define core_h

#include "core_def.h"
#include "core_types.h"

#ifdef __cplusplus
extern "C" {
#endif
// don't want windows.h included in the headers, so redefine handle.

// THREAD
typedef void* THREADHANDLE;
typedef uint32_t THREADID;

THREADID core_thread_currentthreadid();
THREADHANDLE core_thread_gethandletoid(THREADID th);

typedef int (*core_thread_threadproc)(void* context);
THREADHANDLE core_thread_create(core_thread_threadproc proc, void* context);
void core_thread_closehandle(THREADHANDLE handle);
void core_thread_suspend(THREADHANDLE handle);
void core_thread_resume(THREADHANDLE handle);
int core_thread_wait(THREADHANDLE handle, int timeoutms);
void core_thread_sleep(int timeoutms);

// GC

typedef void* core_gc_reflectioninfo;
typedef void (*core_gc_finalizer_sig)(void* obj);

typedef struct core_gc_typeinfo {
	core_gc_reflectioninfo info;
	void* interfacetable;
	void* parenttype;
	// VMT
	core_gc_finalizer_sig finalizer;
} core_gc_typeinfo;


void core_gc_default_finalizer(void*);

#define CORE_GC_FINALIZER_OFFSET sizeof(core_gc_typeinfo);

void core_gc_initialize();
void core_gc_shutdown();
void  core_gc_registercurrentthread();
void core_gc_unregistercurrentthread();
void* core_gc_alloc(core_gc_typeinfo* typeinfo, size_t size);
void core_gc_collect();

#ifdef __cplusplus
}
#endif

#endif