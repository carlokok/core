#include "Windows.h"
#include "core.h"
#include "core_types.h"


int WaitHandle_WaitOne_internal(void* self, HANDLE handle, int timeoutms, bool exitctx) 
{
	return WaitForSingleObject(handle, timeoutms) == WAIT_OBJECT_0;
}


HANDLE Mutex_CreateMutex_internal(bool initiallyOwned, GCString* name, bool* created) 
{
	HANDLE res = CreateMutex(NULL, initiallyOwned, name ? &name->data : NULL);
	*created = res != NULL;
	return res;
}
int Mutex_ReleaseMutex_internal(HANDLE handle) 
{
	return ReleaseMutex(handle);
}

void NativeEventCalls_CloseEvent_internal(HANDLE handle) 
{
	CloseHandle(handle);
}

int NativeEventCalls_SetEvent_internal(HANDLE handle) 
{
	return SetEvent(handle);
}


HANDLE NativeEventCalls_CreateEvent_internal(bool manual, bool initial, GCString* name, bool* created)
{
	HANDLE res = CreateEventA(NULL, manual, initial, name ? &name -> data : NULL);
	*created = res != NULL;
	return res;
}
