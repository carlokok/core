#ifdef USE_BOEHM
#include "core.h"
#include "gc.h"
#include "assert.h"


void  core_gc_registercurrentthread() {
	struct GC_stack_base sb;
	GC_get_stack_base(&sb);
	GC_register_my_thread(&sb);
}
void core_gc_unregistercurrentthread() {
	GC_unregister_my_thread();
}


void GC_InternalCollect(uint32_t gc) {
	int i;
	for (i = 0; i < gc; i++)
		core_gc_collect();
}

void GC_finalizer(void* obj, void* d) {
	core_gc_typeinfo* type = *((core_gc_typeinfo**)obj);
	type->finalizer(obj);
}
void* core_gc_alloc(core_gc_typeinfo* typeinfo, size_t size) {
	assert(size >= sizeof(typeinfo));
	assert(typeinfo != NULL);
	void* res = GC_malloc(size);
	*((void**)res) = typeinfo;
	if (typeinfo->finalizer != NULL && typeinfo->finalizer != core_gc_default_finalizer) {
		GC_register_finalizer_no_order(res, GC_finalizer, NULL, NULL, NULL);
	}
	return res;
}

void GC_SuppressFinalize(GCObject* obj) 
{ 
	if (obj)
		GC_register_finalizer_no_order(obj, NULL, NULL, NULL, NULL);
}


void core_gc_default_finalizer(void* value) {
	// DO NOTHING.
}

void core_gc_initialize()
{
	GC_INIT();
}
void core_gc_shutdown()
{
	
}

void core_gc_collect() {
	GC_gcollect();
}


#endif