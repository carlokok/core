#include <stdio.h>
#include <core.h>

void doit2() { 
	printf("GOT THIS 2 %x\n", (uint32_t)gc_alloc(64)); // 5 blocks as a sizeof(void*) gets added
}

int main(int argc, char **argv)
{
	gc_startup();
	gc_registercurrentthread();
	printf("Hello World\n");	
	
	void* data = gc_alloc(30); // 3 blocks as a sizeof(void*) gets added
	*((void**)data) = data;
	printf("GOT THIS GOT THIS %p at %p\n", data, &data);
	
	doit2();
	
	
	gc_trigger();
	
	getchar();
	
    printf("Hello World\n");
    return 0;
}
 