.PHONY: clean All

All:
	@echo "----------Building project:[ libcore - Debug ]----------"
	@cd "libcore" && $(MAKE) -f  "libcore.mk"
clean:
	@echo "----------Cleaning project:[ libcore - Debug ]----------"
	@cd "libcore" && $(MAKE) -f  "libcore.mk" clean
