// low level garbage collector
// MIT licensed

#ifndef core_gc_h
#define core_gc_h

#include "core_def.h"
#include "core_thread.h"
#ifdef __cplusplus
#include <atomic>
extern volatile bool gc_ingc;
extern "C" {
#endif
typedef void* GCData;


GCData gc_alloc(size_t totalsize);

#ifdef _DEBUG
void gc_dump_arenas();
void gc_reset_object_to_gray(GCData obj);

inline void WRITE_BARRIER(void* obj) 
{ 
	if (!gc_ingc) {
		gc_reset_object_to_gray(obj);
	}
}

#endif


// obj cannot have side effects

void gc_trigger(); // force a gc run
void gc_startup(); // allocate a thread for the gc and stuff
void gc_cleanup(); // wipes tables and stuff; also kills the gc thread


#ifdef _DEBUG
void gc_run();
#endif

// Use these two to register a thread with the garbage collector (will do a stack scan)
void gc_registerthread(uint32_t thread);
void gc_unregisterthread(uint32_t thread);
void gc_registercurrentthread();
void gc_unregistercurrentthread();
// Use these two to lock/unlock an object; a locked object will never be gc'd
void gc_lockobject(GCData object);
void gc_unlockobject(GCData object);

#ifdef __cplusplus
}
#endif
 
#endif 