#ifndef core_types_h
#define core_types_h

#include "core_def.h"
 
typedef struct GCObject 
{
	void* typeinfo;
} GCObject;

typedef struct GCString
{
	GCObject base;
	uint32_t length;
	char data;
} GCString;

typedef struct GCBox {
	GCObject base;
	uint8_t data; // real data goes here, this field is of a random size.
} GCBox;

typedef struct GCArray
{
	GCObject base;
	uint32_t elementsize;
	uint32_t rank;
	intptr_t firstlength;
	void* data;
} GCArray;

#endif