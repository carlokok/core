#ifndef vector_h
#define vector_h
#include "assert.h"

template<typename T>
class linkedlistnode
{
public:
	T value;
	linkedlistnode<T>* next;
};

template<typename T>
class myvector
{
public:
	myvector()
		: m_length(0),
		m_capacity(0),
		m_values(NULL)
	{
	}
	~myvector()
	{
			delete[] m_values;
	}
	bool push_back(const T value) {
		if (m_length +1 > m_capacity) if (!grow(m_length +1)) return false;
		m_values[m_length] = value;
		m_length++;
		return true;
	}
	
	void clear() {
		for (int i = m_length -1; i >= 0; i--) {
			m_values[i] = T();
		}
		m_length = 0;
	}
	
	void remove(const T value) {
		for (int i = 0 ; i < m_length; i++) {
			if (m_values[i] == value) {
				erase(i);
				break;
			}
		}
	}
	
	void erase(int index){
		for (int j = index +1; j < m_length; j++) {
			m_values[j -1] = m_values[j]; 
		}
		m_length --;
	}
	size_t length() const { return m_length; }
	T* pointer() const { return m_values; }
	
	T& operator[] (int idx) const {
		assert(idx >= 0 && idx <m_length);
		return m_values[idx];
	}
	
	bool grow(int minsize) {
		if (minsize < 16) minsize = 16;
		if (minsize <= m_capacity) return true;
		minsize = (minsize -1) * 2;
		T* newarray = new T[minsize];
		if (!newarray) return false;
		m_capacity = minsize;
		for (int i = 0; i < m_length; i++)
			newarray[i] = m_values[i];
		delete[] m_values;
		m_values = newarray;
		return true;
	}
private:
	

	T* m_values;
	size_t m_capacity, m_length;
	myvector(const myvector& in) {}
};

#endif // vector_h
