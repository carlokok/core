#include "windows.h"
#include "core_thread.h"
#include "assert.h"
#include "stdio.h"

#if defined(_WIN32) || defined(_WIN64)

uint32_t CoreGetCurrentThreadID()
{
	return GetCurrentThreadId();
}

THREADHANDLE CoreOpenThreadHandle(uint32_t th)
{
	return OpenThread(THREAD_ALL_ACCESS, FALSE, th);
}

typedef struct CoreThreadData {
	CoreThreadProc proc;
	void* context;
	} CoreThreadData;

DWORD WINAPI CoreStartProc(LPVOID value) {
	CoreThreadData* data = (CoreThreadData*)value;
	assert(data && "data not set");
	CoreThreadProc proc = data->proc;
	void* context = data->context;
	delete data;
	ExitThread(proc(context));
}

THREADHANDLE core_thread_create(CoreThreadProc proc, void* context){
	assert(proc && "no proc set");
	CoreThreadData* data = new CoreThreadData();
	data->proc = proc;
	data->context = context;
	return CreateThread(NULL, 0, CoreStartProc, data, CREATE_SUSPENDED, NULL);
}

  
void core_thread_closehandle(THREADHANDLE handle)
{
	printf("Closing thread handle: %d\n", handle);
	CloseHandle(handle);
}
	
void core_thread_suspend(THREADHANDLE handle)
{
	SuspendThread(handle);
}
void core_thread_resume(THREADHANDLE handle)
{
	ResumeThread(handle);
}	

bool core_thread_wait(THREADHANDLE handle, int timeoutms)
{
	return WAIT_OBJECT_0 == WaitForSingleObject(handle, timeoutms < 0 ? INFINITE: timeoutms);
}

void core_thread_sleep(int timeoutms)
{
	Sleep(timeoutms);
}


#endif