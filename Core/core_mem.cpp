#include "core_mem.h"
#include "stdlib.h"
#include "assert.h"
#include "string.h"
#include "stdio.h"

#if defined(_WIN32) || defined(_WIN64)
#include "Windows.h"

void GCArenaStart::dump() {
	int bytesInBitmap = this->bytesInBitmap();
	uint8_t* data = ((uint8_t*)this) + bytesInBitmap / 64;
	// 8
	// for size 65536: 1024 is used for the marker
	for (int i = bytesInBitmap -1; i >= bytesInBitmap / 64; i--) {
		uint8_t val = *data;
		for (int j = 0; j < 8; j++ ) {
			bool block = 0 != (val & 1 << j);
			bool mark = 0 != (data[bytesInBitmap] & 1 << j);

			auto offset = (int32_t)((uint8_t*)data - (uint8_t*)this) / 16;
			uint8_t* ptr = ((uint8_t*)this) + (((uint8_t*)data - (uint8_t*)this) * (16 * 8) + (j * 16));
			if (!(mark && !block)) { // FREE
				
				if (!block) {
					printf("got %p which is extend\n", ptr);
				} else if (mark) {
					printf("got %p which is black\n", ptr);
				} else {
					printf("got %p which is white\n", ptr);
				}
			}
		}
		data ++;
	}
}

void GCArenaStart::markWhite() {
	int bytesInBitmap = this->bytesInBitmap();
	uint8_t* data = ((uint8_t*)this) + bytesInBitmap / 64;
	// 8
	// for size 65536: 1024 is used for the marker
	for (int i = bytesInBitmap -1; i >= bytesInBitmap / 64; i--) {
		uint8_t val = *data;
		if (0 != (val & 1 << 0)) 
			data[bytesInBitmap] &= ~ (1 << 0);
		if (0 != (val & 1 << 1)) 
			data[bytesInBitmap] &= ~( 1 << 1);
		if (0 != (val & 1 << 2)) 
			data[bytesInBitmap] &= ~( 1 << 2);
		if (0 != (val & 1 << 3)) 
			data[bytesInBitmap] &= ~( 1 << 3);
		if (0 != (val & 1 << 4)) 
			data[bytesInBitmap] &= ~( 1 << 4);
		if (0 != (val & 1 << 5)) 
			data[bytesInBitmap] &= ~( 1 << 5);
		if (0 != (val & 1 << 6)) 
			data[bytesInBitmap] &= ~( 1 << 6);
		if (0 != (val & 1 << 7)) 
			data[bytesInBitmap] &= ~( 1 << 7);
		
		data ++;
	}
 }

inline void core_internalfree(void* buffer){
	VirtualFree(buffer, 0, MEM_RELEASE);
}

inline void* core_internalalloc(int size) {
	void* res = VirtualAlloc(NULL, size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	if (!res)
		GetLastError(); // drop the last error
	return res;
}
#endif

// For now, just use malloc and friends
void* core_allocarena(uint32_t size)
{
	assert(!(size & (size - 1)) && "Size must be power of two");
	uint8_t* data = (uint8_t*)core_internalalloc(size);
	memset(data, 0, size / MARK_OFFSET_DIVIDE); // block bitmap
	memset(data + size / MARK_OFFSET_DIVIDE, 0xff, size / MARK_OFFSET_DIVIDE); // mark bitmap
	auto info = new GCArenaInfo();
	((GCArenaStart*)data)->info = info;
	info->bumpoffset = (size / BITMAP_SIZE_DIVIDE);
	info->totalsize = size;
	info->freememory = size - (size / BITMAP_SIZE_DIVIDE);
	
	return data;
} 
void core_releasearena(void* data){
	delete ((GCArenaStart*)data)->info;
	core_internalfree(data);
}

void operator delete[](void* value) {
	free(value);
}
void* operator new[](unsigned int value) {
	return malloc(value);
}

void operator delete(void* value) {
	free(value);
}
void* operator new(unsigned int value) {
	return malloc(value);
}