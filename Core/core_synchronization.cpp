#include "core_synchronization.h"
#include "Windows.h"

#if defined(_WIN32) || defined(_WIN64)

mutex::~mutex() {
}

mutex::mutex() {
	InitializeCriticalSection(&data);
}

void mutex::lock() {
	EnterCriticalSection(&data);
}

void mutex::unlock() {
	LeaveCriticalSection(&data);
}

void autoresetevent::set()
{
	SetEvent(handle);
}

autoresetevent::autoresetevent(bool initialvalue) {
	handle = CreateEvent(NULL, false, initialvalue, NULL);
}
autoresetevent::~autoresetevent() {
	CloseHandle(handle);
}

bool autoresetevent::wait(int timeoutms){
	return WAIT_OBJECT_0 == WaitForSingleObject(handle, timeoutms < 0 ? INFINITE : timeoutms);
}

manualresetevent::manualresetevent(bool initialvalue)  {
	handle = CreateEvent(NULL, true, initialvalue, NULL);
}

manualresetevent::~manualresetevent() {
	CloseHandle(handle);
}

void manualresetevent::set() {
	SetEvent(handle);
}

void manualresetevent::clear() {
	ResetEvent(handle);
}

bool manualresetevent::wait(int timeoutms) {
	return WAIT_OBJECT_0 == WaitForSingleObject(handle, timeoutms < 0 ? INFINITE : timeoutms);
}

#endif