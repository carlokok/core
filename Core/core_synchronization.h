#ifndef core_mutex_h
#define core_mutex_h

#if defined(_WIN32) || defined(_WIN64)
#include "Windows.h"
class mutex {
public:
	~mutex();
	mutex();
	void lock();
	void unlock();
private:
	CRITICAL_SECTION data;
	mutex(mutex& copy) {
		
	}
};

template< class Mutex >
class lock_guard {
public:
	lock_guard(Mutex& themutex) 
	  : mymutex(themutex)
	{
		mymutex.lock();
	}
	~lock_guard() {
		mymutex.unlock();
	}
private:
	lock_guard(const lock_guard& copy);
	lock_guard & operator=(lock_guard const &);

	Mutex& mymutex;
};
typedef HANDLE EVENTHANDLE;
#else
#error FAIL
#endif

class autoresetevent {
public:
	autoresetevent(bool initialvalue);
	~autoresetevent();
	void set();
	bool wait(int timeoutms = 0);
private:
	EVENTHANDLE handle;
	
	autoresetevent(const autoresetevent& copy);
	autoresetevent& operator=(autoresetevent const &);
};

class manualresetevent {
public:
	manualresetevent(bool initialvalue);
	~manualresetevent();
	void set();
	void clear();
	bool wait(int timeoutms = 0);
private:
	EVENTHANDLE handle;
	
	manualresetevent(const autoresetevent& copy);
	manualresetevent& operator=(autoresetevent const &);
};


#endif 