#ifndef core_thread_h
#define core_thread_h

#include <core_def.h>
#ifdef __cplusplus
extern "C" {
#endif

// don't want windows.h included in the headers, so redefine handle.

#if defined(_WIN32) || defined(_WIN64)
typedef void* THREADHANDLE;
#else
#error Platform not supported!
#endif

uint32_t CoreGetCurrentThreadID();
THREADHANDLE CoreOpenThreadHandle(uint32_t th);

typedef int (*CoreThreadProc)(void* context);
THREADHANDLE core_thread_create(CoreThreadProc proc, void* context);
void core_thread_closehandle(THREADHANDLE handle);
void core_thread_suspend(THREADHANDLE handle);
void core_thread_resume(THREADHANDLE handle);
bool core_thread_wait(THREADHANDLE handle, int timeoutms);
void core_thread_sleep(int timeoutms);

#ifdef __cplusplus
}
#endif

#endif