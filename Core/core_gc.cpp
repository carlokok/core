#include "core_gc.h"
#include "core_synchronization.h"
#include "core_vector.h"
#include "core_mem.h"
#include <atomic>
#include <tuple>
#ifdef _DEBUG
#include "stdio.h"
#endif

struct GCThreadData {
public:
	THREADHANDLE thread;
	uint32_t threadid;
	void* stacktop;
};

struct GCTuple {
public:
	GCTuple() {}
	GCTuple(uint8_t* sval, uint8_t* eval, GCArenaStart* aval): start(sval), end(eval), arena(aval) {}
	uint8_t* start;
	uint8_t* end;
	GCArenaStart* arena;
};

mutex arena_lock;

myvector<GCTuple> allarenas; // during a gc this will have all the arenas applicable for gc.

myvector<GCArenaStart*> arena;
mutex thread_list_lock;
myvector<GCThreadData> thread_list;
mutex locked_object_list_lock;
myvector<GCData> locked_object_list;

std::atomic<bool> gc_running(false);
std::atomic<bool> gc_stop;
volatile bool gc_ingc;

autoresetevent gc_threadtrigger(false);

THREADHANDLE gc_thread;

void* gc_get_stack_top(void* s)
{
    MEMORY_BASIC_INFORMATION info;
    VirtualQuery(s, &info, sizeof(info));
	return ((uint8_t*)info.BaseAddress) + info.RegionSize;
}

void* gc_get_stack_for_thread(THREADHANDLE th) {
	#if defined(_WIN32)
	CONTEXT td;
	GetThreadContext(th, &td);
	return (void*)td.Esp;
	#else
	#error FAIL
	#endif	
}
void gc_registerthread(uint32_t thread) 
{
	lock_guard<mutex> lock(thread_list_lock);
	
	GCThreadData td;
	td.threadid = thread;
	td.thread = CoreOpenThreadHandle(thread);
	if (thread == CoreGetCurrentThreadID()) {
		td.stacktop = gc_get_stack_top(__builtin_frame_address(0));
	} else {
		core_thread_suspend(td.thread);
		
		td.stacktop = gc_get_stack_top(gc_get_stack_for_thread(td.thread));
		
		core_thread_resume(td.thread);
	}
	thread_list.push_back(td);
}

void gc_unregisterthread(uint32_t thread)
{
	lock_guard<mutex> lock(thread_list_lock);

	for (int i = 0; i < thread_list.length(); i++) {
		if (thread_list[i].threadid == thread)
		{
			thread_list.erase(i);
			break;
		}
	}
}

void gc_lockobject(GCData object)
{
	lock_guard<mutex> lock(locked_object_list_lock);

	locked_object_list.push_back(object);
}

void gc_unlockobject(GCData object)
{
	lock_guard<mutex> lock(locked_object_list_lock);

	locked_object_list.remove(object);
}

inline GCArenaStart* gc_get_arena_for_object(GCData data) {
	data = (GCData)(((intptr_t) data) &~ 0x15);
	for (int i = allarenas.length() -1; i >= 0; i--) {
		GCTuple& larena = allarenas[i];
		if (data >= larena.start && data <= larena.end) {
			if (larena.arena->isBlockBitSet(data))
				return larena.arena;
		}
	}
	return NULL;
}

void gc_reset_object_to_gray(GCData obj){
	auto arena = gc_get_arena_for_object(obj);
	assert(arena);
	arena->info->push(obj);
}

void gc_mark_gray(GCData data) {
	GCArenaStart* val = gc_get_arena_for_object(data);
	if (val) {
		if (!val->isMarkBitSet(data))
			val->info->push(data);
	}
}

void gc_mark_thread(GCThreadData& th) {
	#if defined(_WIN32) 
		CONTEXT thdata;
		thdata.ContextFlags = CONTEXT_FULL;
 		GetThreadContext(th.thread, &thdata);
		gc_mark_gray((GCData)thdata.Edi);
		gc_mark_gray((GCData)thdata.Esi);
		gc_mark_gray((GCData)thdata.Ebx);
		gc_mark_gray((GCData)thdata.Edx);
		gc_mark_gray((GCData)thdata.Ecx);
		gc_mark_gray((GCData)thdata.Eax);
		
		for (DWORD start = thdata.Esp &~3; start < (DWORD)th.stacktop; start += 4) {
			gc_mark_gray(*((void**)start));
		}
	#else
	#error FAIL
	#endif
}

void gc_mark() {
	bool again = true;
	while (again) {
		again = false;
		for (int i = allarenas.length()-1; i>= 0; i--) {
			while (true) {
				uint8_t* val = (uint8_t*)allarenas[i].arena->info->pop();
				if (!val) break;

				// mark the value as black
				allarenas[i].arena->markBlack(val);
				
				again = true;
				int bitoffset = ((uint8_t*)val - (uint8_t*)allarenas[i].arena ) / 16;
				assert(allarenas[i].arena->isBlockBitSet(bitoffset));
				void** workval = (void**)val;
				do {
					#ifdef IS64BITS
					// 2*8 bytes
					gc_mark_gray((GCData)*workval); workval ++;
					gc_mark_gray((GCData)*workval); workval ++;
					#else 
					// 4 * 4 bytes
					gc_mark_gray((GCData)*workval); workval ++;
					gc_mark_gray((GCData)*workval); workval ++;
					gc_mark_gray((GCData)*workval); workval ++;
					gc_mark_gray((GCData)*workval); workval ++;
					#endif
					bitoffset ++;
					if (!allarenas[i].arena->isBlockExtend(bitoffset)) break;
				} while (true);
			}
		}
	}
}

void gc_free(GCArenaStart* arena, uint8_t* ptr) {
	// release it.
	#ifdef _DEBUG
	printf("freeing %p\n", ptr);
	#endif
	
	// mark as free
	int free = arena->markFree(ptr);
	lock_guard<mutex> lock(arena_lock);
	arena->info->freememory += free;
	
}

void gc_sweep() {
	gc_dump_arenas();
	for (int i = allarenas.length()-1; i>= 0; i--) {
		auto arena = allarenas[i];
		
		int bytesInBitmap = arena.arena ->bytesInBitmap();
		uint8_t* data = ((uint8_t*)arena.arena) + bytesInBitmap / 64;
		// find things with the block bit set; and a WHITE node.
		for (int k = bytesInBitmap -1; k >= bytesInBitmap / 64; k--) {
			uint8_t val = *data;
			
			for (int j = 0; j < 8; j++) {
				if (0 != (val & 1 << j) && 0 == (data[bytesInBitmap] & 1 << j)) { gc_free(arena.arena, ((uint8_t*)arena.arena) + (((uint8_t*)data - (uint8_t*)arena.arena) * (16 * 8) + (j * 16))); } 
			}
			
			data ++;
		}
	}
	gc_dump_arenas();
}

void gc_run() {
	gc_ingc = true;
	{
		lock_guard<mutex> lock(arena_lock);
		if (!allarenas.grow(arena.length()))
			return; // not enough memory for gc
		allarenas.clear();
		for (int i = 0; i < arena.length(); i++) {
			GCArenaStart* st = arena[i];
			GCTuple tpl((uint8_t*)(st + st->info->totalsize/256), (((uint8_t*)st) + st->info->totalsize), st);
			allarenas.push_back(tpl);
		}
	}
	
	// During a GC run you can't start a new thread.
	lock_guard<mutex> th_lock(thread_list_lock);
	for (int i = thread_list.length() -1; i>=0;i --) 
		core_thread_suspend(thread_list[i].thread);
	
	for (int i = thread_list.length() -1; i>=0;i --) 
		gc_mark_thread(thread_list[i]);

	lock_guard<mutex> lock(locked_object_list_lock);
	for (int i = locked_object_list.length() -1; i>= 0; i--)
		gc_mark_gray(locked_object_list[i]);

	gc_dump_arenas();
	// Now we make all objects in the arenas we know about white; at this point we don't care about newly allocated arenas
	for (int i = allarenas.length()-1; i>= 0; i--) {
		allarenas[i].arena->markWhite();
	}
	gc_dump_arenas();
	
	for (int i = thread_list.length() -1; i>=0;i --) 
		core_thread_resume(thread_list[i].thread);
		
	// Now we loop till all gray stacks are empty
	gc_mark();
	
	gc_sweep();
	
	allarenas.clear();
	gc_ingc = false;
}
	
#ifdef _DEBUG
#define BYTETOBINARYPATTERN "%d%d%d%d%d%d%d%d"
#define BYTETOBINARY(byte)  \
  (byte & 0x01 ? 1 : 0), \
  (byte & 0x02 ? 1 : 0), \
  (byte & 0x04 ? 1 : 0), \
  (byte & 0x08 ? 1 : 0), \
  (byte & 0x10 ? 1 : 0), \
  (byte & 0x20 ? 1 : 0), \
  (byte & 0x40 ? 1 : 0), \
  (byte & 0x80 ? 1 : 0) 
  
void gc_printbin(uint8_t* data, int len) {
	while (len > 0) {
		printf(BYTETOBINARYPATTERN, BYTETOBINARY(*data));
		len--;
		data++;
	}
}
void gc_dump_arenas() 
{
	lock_guard<mutex> lock(arena_lock);

	printf("Dumping arena; got %d nrs!\n", arena.length());
	for (int i = 0; i < arena.length(); i++) {
		GCArenaStart* ar = arena[i];
		ar->dump();
	}
}
#endif

GCData gc_alloc_in_arena(GCArenaStart* arena, size_t totalsize) {
	#ifdef _DEBUG
	printf("before\n");
	gc_dump_arenas();
	#endif
	auto info = arena->info;
	totalsize = (totalsize + (CELL_SIZE - 1)) & ~(CELL_SIZE -1);

	assert(totalsize < info->freememory && "total size < free mem");
	
	int bitmapsize = info->totalsize / MARK_OFFSET_DIVIDE;
	int bumpoffset = info->bumpoffset;
	if (bumpoffset == 0) return NULL;

	// first we increase the bump offset, that way it's "used", and decrease the free memory int.
	info->bumpoffset = bumpoffset + totalsize;
	info->freememory -= totalsize;
	
	GCData result = (uint8_t*)arena + bumpoffset;
	
	bumpoffset /= CELL_SIZE;
	
	// set the "block" bit, this will make it a black node.
	*(((uint8_t*)arena) + (bumpoffset / 8)) |= (1 << (bumpoffset % 8));
	
	// mark the first bit as white and rest as block extend; if the gc is running, keep it on black
	for (int i = (totalsize / CELL_SIZE) -1; i >= gc_ingc ? 1 : 0; i--)  {
		*(((uint8_t*)arena) + bitmapsize + ((bumpoffset + i) / 8)) &= ~(1 << ((bumpoffset + i)% 8));
	}
	
	#ifdef _DEBUG
	printf("returned ptr: %result\n");
	gc_dump_arenas();
	#endif
	return result;
}

int gc_main(void* dummy) {
	do {
		gc_threadtrigger.wait(-1);
		if (gc_stop) return 0;
		gc_run();
	} while (true);
}

void gc_startup()
{
	if (gc_running.exchange(true)) return; // already running.
	gc_stop = false;
	
		gc_thread = core_thread_create(gc_main, NULL);
	core_thread_resume(gc_thread);
}

void gc_cleanup()
{
	if (gc_running.exchange(false)) {
		gc_stop = true;
		gc_threadtrigger.set();
		core_thread_wait(gc_thread, -1);
		core_thread_closehandle(gc_thread);
		gc_thread = 0;
	}
	
	lock_guard<mutex> lock(locked_object_list_lock);
	thread_list.clear();
}

void gc_trigger()
{
	gc_threadtrigger.set();
}


inline size_t gc_nearestsize(size_t i) {
	if (i < 1 << 16) return DEFAULT_ARENA_SIZE;
	#ifdef IS64BITS
	if (i >= 1 << 32) {
		if (i > 1 << 47) {
		  if (i & (1 << 63)) return 1 << 63;
		  if (i & (1 << 62)) return 1 << 62;
		  if (i & (1 << 61)) return 1 << 61;
		  if (i & (1 << 60)) return 1 << 60;
		  if (i & (1 << 59)) return 1 << 59;
		  if (i & (1 << 58)) return 1 << 58;
		  if (i & (1 << 56)) return 1 << 56;
		  if (i & (1 << 55)) return 1 << 55;
		  if (i & (1 << 54)) return 1 << 54;
		  if (i & (1 << 53)) return 1 << 53;
		  if (i & (1 << 52)) return 1 << 52;
		  if (i & (1 << 51)) return 1 << 51;
		  if (i & (1 << 50)) return 1 << 50;
		  if (i & (1 << 49)) return 1 << 49;
		  if (i & (1 << 48)) return 1 << 48;
		  return 1 << 47; 
		} 
	  
	  if (i & (1 << 46)) return 1 << 46;
	  if (i & (1 << 45)) return 1 << 45;
	  if (i & (1 << 44)) return 1 << 44;
	  if (i & (1 << 43)) return 1 << 43;
	  if (i & (1 << 42)) return 1 << 42;
	  if (i & (1 << 41)) return 1 << 41;
	  if (i & (1 << 40)) return 1 << 40;
	  if (i & (1 << 39)) return 1 << 39;
	  if (i & (1 << 38)) return 1 << 38;
	  if (i & (1 << 37)) return 1 << 37;
	  if (i & (1 << 36)) return 1 << 36;
	  if (i & (1 << 35)) return 1 << 35;
	  if (i & (1 << 34)) return 1 << 34;
	  if (i & (1 << 33)) return 1 << 33;
	  if (i & (1 << 32)) return 1 << 32;
	}
	#endif
    if (i >= (1 << 24)) {
	  if (i & (1 << 31)) return 1 << 31; // this is so going to fail.
	  if (i & (1 << 30)) return 1 << 31;
	  if (i & (1 << 29)) return 1 << 30;
	  if (i & (1 << 28)) return 1 << 29;
	  if (i & (1 << 27)) return 1 << 28;
	  if (i & (1 << 26)) return 1 << 27;
	  if (i & (1 << 25)) return 1 << 26;
	  if (i & (1 << 24)) return 1 << 25;
	}
	if (i & (1 << 23)) return 1 << 24;
	if (i & (1 << 22)) return 1 << 23;
	if (i & (1 << 21)) return 1 << 22;
	if (i & (1 << 20)) return 1 << 21;
	if (i & (1 << 19)) return 1 << 20;
	if (i & (1 << 18)) return 1 << 19;
	if (i & (1 << 17)) return 1 << 18;
	if (i & (1 << 16)) return 1 << 17;
  return 1 << 16;	
}

GCData gc_alloc(size_t totalsize)
{
	// totalsize += sizeof(void*); // add a pointer for GC data.
	assert(totalsize <= MAX_OBJECT_SIZE && "Object too big, should go into the big heap, but we don't have one yet!");
	
	lock_guard<mutex> lock(arena_lock);
	
	int length = arena.length();
	GCArenaStart** element = &arena.pointer()[0];
	// first find an arena that could potentially fit it
	while (length) {
		if (totalsize <= (*element)->info->freememory) {
			GCData value = gc_alloc_in_arena((*element), totalsize);
			if (value)
				return value;
		}
		element ++;
		length --;
	}
	// none of the arenas has this available, try allocating a new one.
	int allocsize = gc_nearestsize(totalsize + ((totalsize + 63) / 64));
	GCArenaStart* newarena = (GCArenaStart*)core_allocarena(allocsize);
	if (!newarena) return NULL;
	if (!arena.push_back(newarena)) {
		core_releasearena(newarena);
		return NULL;
	}
	// presuming we have enough memory for this, so allocate it
	return gc_alloc_in_arena(newarena, totalsize);
}
void gc_registercurrentthread()
{
	gc_registerthread(CoreGetCurrentThreadID());
}
void gc_unregistercurrentthread()
{
	gc_unregisterthread(CoreGetCurrentThreadID());
}
