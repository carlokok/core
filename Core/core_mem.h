#ifndef core_mem_h
#define core_mem_h

#include "core_def.h"

#ifdef __cplusplus
#include "core_lfstack.h"
// Following this info: http://wiki.luajit.org/New-Garbage-Collector

#define DEFAULT_ARENA_SIZE (1024 * 64)
#define BITMAP_SIZE_DIVIDE 64
#define BLOCK_OFFSET 0
#define CELL_SIZE 16
#define MARK_OFFSET_DIVIDE 128
#define MAX_BLOCK_SIZE (1024 * 1024)
#define MAX_OBJECT_SIZE (MAX_BLOCK_SIZE - (MAX_BLOCK_SIZE / BITMAP_SIZE_DIVIDE))

struct GCArenaInfo: public lockfreestack<void*> {
public:
	uint32_t freememory;
	uint32_t totalsize;
	uint32_t bumpoffset;
};

struct GCArenaStart {
public: // at the very least we have 16 bytes (64k block); this means 1 pointer (8 or 4) then two 4 byte ints.
	 GCArenaInfo* info;
	 bool isBlockBitSet(void* data) {
		 assert(data >= (uint8_t*)this + (info->totalsize / 64));
		 assert(data < ((uint8_t*)this + info->totalsize));
		 auto offset = (int32_t)((uint8_t*)data - (uint8_t*)this) / 16;
		 return 0 != (((uint8_t*)this)[offset / 8] & (1 << (offset & 7)));
	 }
	 
	 int isBlockBitSet(int offset) {
		 assert(offset >= bytesInBitmap() / 64 && offset < bytesInBitmap() * 8);
		 return (((uint8_t*)this)[offset / 8] & (1 << (offset & 7)));
	 }

	 int isMarkBitSet(int offset) {
		 assert(offset >= bytesInBitmap() / 64 && offset < bytesInBitmap() * 8);
		 return (((uint8_t*)this)[bytesInBitmap() + offset / 8] & (1 << (offset & 7)));
	 }
	 
	 bool isBlockExtend(int offset) {
		 assert(offset >= bytesInBitmap() / 64 && offset < bytesInBitmap() * 8);
		 return !(((uint8_t*)this)[offset / 8] & (1 << (offset & 7))) &&
		 !(((uint8_t*)this)[bytesInBitmap() + offset / 8] & (1 << (offset & 7)));
	 }
	 
	 int  markFree(void* data) {
		 assert(data >= (uint8_t*)this + (info->totalsize / 64));
		 assert(data < ((uint8_t*)this + info->totalsize));
		 auto offset = (int32_t)((uint8_t*)data - (uint8_t*)this) / 16;
			
		assert(isBlockBitSet(offset)); 
		
		auto bb = bytesInBitmap();
		// unset the block bit
		((uint8_t*)this)[offset / 8] &= ~(1 << (offset & 7));
		((uint8_t*)this)[bb + offset / 8] |= (1 << (offset & 7));
		offset++;
		int res = 16;
		while (offset < bb * 8) {
			if (!isBlockExtend(offset)) break;
			// block extend is 0 0; set it to 0 1
			((uint8_t*)this)[bb + offset / 8] |= (1 << (offset & 7));
			offset++;
			res += 16;
		}
		return res;
	 }
	 
	 void markBlack(void* data) {
		 assert(data >= (uint8_t*)this + (info->totalsize / 64));
		 assert(data < ((uint8_t*)this + info->totalsize));
		 auto offset = (int32_t)((uint8_t*)data - (uint8_t*)this) / 16;

		 assert(isBlockBitSet(offset));
		 
		 ((uint8_t*)this)[bytesInBitmap() + offset / 8] |= (1 << (offset & 7));
	 }
	 
	 void markWhite(void* data) {
		 assert(data >= (uint8_t*)this + (info->totalsize / 64));
		 assert(data < ((uint8_t*)this + info->totalsize));
		 auto offset = (int32_t)((uint8_t*)data - (uint8_t*)this) / 16;

		 assert(isBlockBitSet(offset));
		 
		 ((uint8_t*)this)[bytesInBitmap() + offset / 8] &= ~(1 << (offset & 7));
	 }

	 
	 bool isMarkBitSet(void* data) {
		 assert(data >= (uint8_t*)this + (info->totalsize / 64));
		 assert(data < ((uint8_t*)this + info->totalsize));
		 auto offset = (int32_t)((uint8_t*)data - (uint8_t*)this) / 16;
		 
		 return 0 != (((uint8_t*)this)[bytesInBitmap() + offset / 8] & (1 << (offset & 7)));
	 }
	 
	 // Bytes in a single bitmap
	 inline int bytesInBitmap() {
		 return info->totalsize / 128;
	 }
	 
	 // Set ALL used objects to white.
	 void markWhite();	 
	 void dump();
};

extern "C" {

#endif


void* core_allocarena(size_t size);
void core_releasearena(void* buffer);

#ifdef __cplusplus
}

void operator delete[](void* value);
void* operator new[](unsigned int value);
void operator delete(void* value);
void* operator new(unsigned int value);
#endif

#endif