#ifndef core_h
#define core_h

#include "core_def.h"
#include "core_types.h"
#include "core_thread.h"
#include "core_mem.h"
#include "core_gc.h"

#endif