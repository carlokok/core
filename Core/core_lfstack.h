#ifndef core_lfstack_h
#define core_lfstack_h

#include "core.h"
#include <intrin.h>
#if defined(_WIN32) || defined(_WIN64)
#include "windows.h"
#endif


template<typename T>
class lockfreestack
{
private:

	//template<typename T>
	struct lockfreestacknode {
	public:
		lockfreestack<T>::lockfreestacknode* next;
		T value;
	};
	struct lockfreestackhead {
	public:
		lockfreestack<T>::lockfreestacknode* root;
		intptr_t counter;
		
		bool operator == (lockfreestack<T>::lockfreestackhead& r) {
			return root == r.root && counter == r.counter;
		}
	};

	lockfreestack<T>::lockfreestackhead head;

	lockfreestack(const lockfreestack<T>& rhs);
	lockfreestack& operator=(const lockfreestack<T>& rhs);

    // compares *head to *compare, if true, returns true and sets *head to newvla
	static inline bool compareexchange(lockfreestackhead* head, lockfreestackhead newval, lockfreestackhead* compareandresult)
	{
		#ifdef _WIN32
		lockfreestackhead oldvalue = *compareandresult;
		*((LONGLONG*)compareandresult) = InterlockedCompareExchange64((LONGLONG*)head, *((LONGLONG*)&newval), *((LONGLONG*)&oldvalue));
		return *compareandresult == oldvalue;
		#elif _WIN64
		
		return 0 != InterlockedCompareExchange128((LONGLONG*)head, *((LONGLONG*)&newval.root), *((LONGLONG*)&newval.counter), (LONGLONG*)compareandresult);
		#else
		#error FAIL
		#endif
	}
	
public:
	lockfreestack()
	{
		head.counter = 0;
		head.root = NULL;
	}
	
	~lockfreestack()
	{
	}
	
	void push(T& value) {
		lockfreestacknode* newnode = new lockfreestacknode();
		newnode->value = value;
		
		lockfreestackhead oldhead = head;
		lockfreestackhead newhead;
		newhead.root = newnode;
		do {
			newhead.counter = oldhead.counter + 1;
			newnode->next = oldhead.root;
		} while (!compareexchange(&head, newhead, &oldhead));
	}
	
	T pop() {
		lockfreestackhead oldhead = head;
		lockfreestackhead newhead;
        do {
			if (!oldhead.root)
				return T();
			newhead.root = oldhead.root->next;
            newhead.counter = oldhead.counter + 1;
		} while (!compareexchange(&head, newhead, &oldhead));
		
		
		T res = oldhead.root->value;
		delete oldhead.root;
        return res;
    }
};

#endif // core_lfstack_h