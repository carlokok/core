##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=Core
ConfigurationName      :=Debug
WorkspacePath          := "c:\projects\Core"
ProjectPath            := "c:\projects\Core\Core"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=carlokok
Date                   :=01/27/14
CodeLitePath           :="C:\Program Files (x86)\CodeLite"
LinkerName             :=g++
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/lib$(ProjectName).a
Preprocessors          :=$(PreprocessorSwitch)_DEBUG 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="Core.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=windres
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := g++
CC       := gcc
CXXFLAGS :=  -g -std=c++11 -fno-exceptions -fno-rtti $(Preprocessors)
CFLAGS   :=  -g $(Preprocessors)
ASFLAGS  := 
AS       := as


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files (x86)\CodeLite
UNIT_TEST_PP_SRC_DIR:=C:\UnitTest++-1.3
Objects0=$(IntermediateDirectory)/core_gc$(ObjectSuffix) $(IntermediateDirectory)/core_mem$(ObjectSuffix) $(IntermediateDirectory)/core_thread$(ObjectSuffix) $(IntermediateDirectory)/core_synchronization$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild
all: $(IntermediateDirectory) $(OutputFile)

$(OutputFile): $(Objects)
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(AR) $(ArchiveOutputSwitch)$(OutputFile) @$(ObjectsFileList) $(ArLibs)
	@$(MakeDirCommand) "c:\projects\Core/.build-debug"
	@echo rebuilt > "c:\projects\Core/.build-debug/Core"

./Debug:
	@$(MakeDirCommand) "./Debug"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/core_gc$(ObjectSuffix): core_gc.cpp $(IntermediateDirectory)/core_gc$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "c:/projects/Core/Core/core_gc.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/core_gc$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/core_gc$(DependSuffix): core_gc.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/core_gc$(ObjectSuffix) -MF$(IntermediateDirectory)/core_gc$(DependSuffix) -MM "core_gc.cpp"

$(IntermediateDirectory)/core_gc$(PreprocessSuffix): core_gc.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/core_gc$(PreprocessSuffix) "core_gc.cpp"

$(IntermediateDirectory)/core_mem$(ObjectSuffix): core_mem.cpp $(IntermediateDirectory)/core_mem$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "c:/projects/Core/Core/core_mem.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/core_mem$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/core_mem$(DependSuffix): core_mem.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/core_mem$(ObjectSuffix) -MF$(IntermediateDirectory)/core_mem$(DependSuffix) -MM "core_mem.cpp"

$(IntermediateDirectory)/core_mem$(PreprocessSuffix): core_mem.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/core_mem$(PreprocessSuffix) "core_mem.cpp"

$(IntermediateDirectory)/core_thread$(ObjectSuffix): core_thread.cpp $(IntermediateDirectory)/core_thread$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "c:/projects/Core/Core/core_thread.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/core_thread$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/core_thread$(DependSuffix): core_thread.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/core_thread$(ObjectSuffix) -MF$(IntermediateDirectory)/core_thread$(DependSuffix) -MM "core_thread.cpp"

$(IntermediateDirectory)/core_thread$(PreprocessSuffix): core_thread.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/core_thread$(PreprocessSuffix) "core_thread.cpp"

$(IntermediateDirectory)/core_synchronization$(ObjectSuffix): core_synchronization.cpp $(IntermediateDirectory)/core_synchronization$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "c:/projects/Core/Core/core_synchronization.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/core_synchronization$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/core_synchronization$(DependSuffix): core_synchronization.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/core_synchronization$(ObjectSuffix) -MF$(IntermediateDirectory)/core_synchronization$(DependSuffix) -MM "core_synchronization.cpp"

$(IntermediateDirectory)/core_synchronization$(PreprocessSuffix): core_synchronization.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/core_synchronization$(PreprocessSuffix) "core_synchronization.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) $(IntermediateDirectory)/core_gc$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/core_gc$(DependSuffix)
	$(RM) $(IntermediateDirectory)/core_gc$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/core_mem$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/core_mem$(DependSuffix)
	$(RM) $(IntermediateDirectory)/core_mem$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/core_thread$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/core_thread$(DependSuffix)
	$(RM) $(IntermediateDirectory)/core_thread$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/core_synchronization$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/core_synchronization$(DependSuffix)
	$(RM) $(IntermediateDirectory)/core_synchronization$(PreprocessSuffix)
	$(RM) $(OutputFile)
	$(RM) $(OutputFile)
	$(RM) "../.build-debug/Core"


